package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {

    /*
     * 分页查询供应商
     * */
    Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName);

    /*
     * 新增供应商
     * */
    ServiceVO save(Supplier supplier);

    /*
     * 删除供应商
     * */
    ServiceVO delete(String ids);
}
