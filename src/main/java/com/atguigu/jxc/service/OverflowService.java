package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;

public interface OverflowService {

    /*
     * 保存报溢单
     * */
    ServiceVO overflowListGoods(OverflowList overflowList, String overflowListGoodsStr);

    /*
     * 报溢单查询
     * */
    Map<String, Object> getOverflowList(String sTime, String eTime);

    /*
     * 报溢单商品信息
     * */
    Map<String, Object> getOverflowListGoods(Integer overflowListId);
}
