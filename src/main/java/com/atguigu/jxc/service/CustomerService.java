package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {

    /*
     * 获取客户列表（分页查询）
     * */
    Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName);

    /*
    *  新增/更新客户
    * */
    ServiceVO save(Customer customer);

    /*
    * 删除客户
    * */
    ServiceVO delete(String ids);
}
