package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;

import java.util.Map;

public interface DamageService {

    /*
     * 保存报损单
     * */
    ServiceVO damageListGoodsSave(DamageList damageList, String damageListGoodsStr);

    /*
     * 报损单查询
     * */
    Map<String, Object> damageListGoods(String sTime, String eTime);

    /*
     * 查询报损单商品信息
     * */
    Map<String, Object> getGoodsList(Integer damageListId);
}
