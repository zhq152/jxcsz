package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

import java.util.ArrayList;

/**
 * @description
 */
public interface GoodsTypeService {

    /**
     * 查询所有商品类别
     *
     * @return easyui要求的JSON格式字符串
     */
    ArrayList<Object> loadGoodsType();

    /*
     * 新增商品分类
     * */
    ServiceVO addGoodsType(String goodsTypeName, Integer pId);

    /*
     *删除商品分类
     */
    ServiceVO deleteGoodsType(Integer goodsTypeId);
}
