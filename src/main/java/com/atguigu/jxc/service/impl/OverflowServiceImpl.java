package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.OverflowDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowServiceImpl implements OverflowService {


    @Autowired
    private OverflowDao overflowDao;

    /*
     * 保存报溢单
     * */
    @Override
    public ServiceVO overflowListGoods(OverflowList overflowList, String overflowListGoodsStr) {
        overflowDao.overflowList(overflowList);
        //取返回的ID
        Integer overflowListId = overflowList.getOverflowListId();

        if (overflowListId != null) {
            //保存报损单下面的物品列表
            List<OverflowListGoods> overflowListGoods = JSON.parseArray(overflowListGoodsStr, OverflowListGoods.class);
            for (OverflowListGoods overflowListGood : overflowListGoods) {
                overflowListGood.setOverflowListId(overflowListId);
                overflowDao.overflowListGoods(overflowListGood);
            }
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    /*
     * 报溢单查询
     * */
    @Override
    public Map<String, Object> getOverflowList(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();

        List<OverflowList> overflowList = overflowDao.getOverflowList(sTime, eTime);

        map.put("rows", overflowList);

        return map;
    }

    /*
     * 报溢单商品信息
     * */
    @Override
    public Map<String, Object> getOverflowListGoods(Integer overflowListId) {

        HashMap<String, Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoodsList = overflowDao.getOverflowListGoods(overflowListId);
        map.put("rows", overflowListGoodsList);
        return map;
    }
}
