package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageService;
import org.apache.shiro.crypto.hash.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@SuppressWarnings("ALL")
public class DamageServiceImpl implements DamageService {

    @Autowired
    private DamageDao damageDao;

    /*
     * 保存报损单
     * */
    @Override
    public ServiceVO damageListGoodsSave(DamageList damageList, String damageListGoodsStr) {

        /*
         *
         * */
        damageDao.damageListSave(damageList);
        //取返回的ID
        Integer damageListId = damageList.getDamageListId();

        //保存报损单下面的物品列表
        List<DamageListGoods> damageListGoods = JSON.parseArray(damageListGoodsStr, DamageListGoods.class);
        for (DamageListGoods damageListGood : damageListGoods) {
            damageListGood.setDamageListId(damageListId);
            damageDao.damageListGoodsSave(damageListGood);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

       /* if (rows > 0) {
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        }*/

    }

    /*
     * 报损单查询
     * */
    @Override
    public Map<String, Object> damageListGoods(String sTime, String eTime) {

        HashMap<String, Object> map = new HashMap<>();

        List<DamageList> damageListList = damageDao.damageList(sTime, eTime);

        map.put("rows", damageListList);

        return map;
    }

    /*
     * 查询报损单商品信息
     * */
    @Override
    public Map<String, Object> getGoodsList(Integer damageListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<DamageListGoods> damageListGoodsList = damageDao.getGoodsList(damageListId);
        map.put("rows", damageListGoodsList);
        return map;
    }

}
