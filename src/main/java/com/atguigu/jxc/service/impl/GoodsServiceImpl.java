package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
@SuppressWarnings("all")
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> getListInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        HashMap<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        //分页查询商品
    /*    List<Goods> goodsList = null;
        if (codeOrName != null || goodsTypeId != null) {
            //带条件的查询
            goodsList = goodsDao.getGoodsList(offSet, rows, codeOrName, goodsTypeId);
        } else {
            //进首页第一次不带条件查询
            goodsList = goodsDao.getGoodsListAll(offSet, rows);
        }*/

        //List<Goods> goodsChildList = goodsDao.getGoodsChild(codeOrName, goodsTypeId);

        List<Goods> goodsList = goodsDao.getGoodsList(offSet, rows, codeOrName, goodsTypeId);

        int size = 0;
        if (goodsList != null) {
            for (Goods goods : goodsList) {
                Integer goodsId = goods.getGoodsId();
                //聚合查询销售总数
                Integer GoodsSaleTotalNum = goodsDao.getGoodsSaleTotalNum(goodsId);
                //聚合查询客户回退商品总数
                Integer CustomerReturnGoodsTotalNum = goodsDao.getCustomerReturnGoodsTotalNum(goodsId);
                if (GoodsSaleTotalNum == null) {
                    GoodsSaleTotalNum = 0;
                    goods.setSaleTotal(GoodsSaleTotalNum);
                }
                if (CustomerReturnGoodsTotalNum == null) {
                    goods.setSaleTotal(GoodsSaleTotalNum);

                }
                if (GoodsSaleTotalNum != null && CustomerReturnGoodsTotalNum != null) {
                    Integer saleResult = GoodsSaleTotalNum - CustomerReturnGoodsTotalNum;
                    goods.setSaleTotal(saleResult);
                }
                /*if (saleTotal == 0) {
                    goods.setSaleTotal(0);
                }*/
            }
            size = goodsList.size();
        }
        map.put("total", size);
        map.put("rows", goodsList);

        return map;
    }


    public static void main(String[] args) {
        Integer i = 10;
        Integer j = 5;
        Integer s = i - j;
        System.out.println(s.TYPE);
        Goods goods = new Goods();
        goods.setSaleTotal(s);
        System.out.println(goods.getSaleTotal());
    }

    @Override
    public Map<String, Object> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        HashMap<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        //分页查询商品
        /*List<Goods> goodsList = goodsDao.getGoodsList(offSet, rows, goodsName, goodsTypeId);*/
        List<Goods> goodsList = null;
        if (goodsTypeId != null) {
            if (goodsTypeId == 1) {
                goodsTypeId = 0;
                goodsList = goodsDao.getGoodsList(offSet, rows, goodsName, goodsTypeId);
            } else {
                goodsList = goodsDao.getGoodsList(offSet, rows, goodsName, goodsTypeId);
            }
        } else {
            goodsList = goodsDao.getGoodsList(offSet, rows, goodsName, goodsTypeId);
        }

        int size = 0;
        if (goodsList != null) {
            size = goodsList.size();
        }

        map.put("total", size);
        map.put("rows", goodsList);

        return map;
    }

    /*
     * 查询所有商品单位
     * */
    @Override
    public Map<String, Object> getUnitList() {
        HashMap<String, Object> map = new HashMap<>();

        List<Unit> unitList = goodsDao.getUnitList();
        map.put("rows", unitList);
        return map;
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     * @return
     */
    @Override
    public ServiceVO goodsSave(Goods goods) {
        if (goods.getInventoryQuantity() == null && goods.getState() == null) {
            goods.setInventoryQuantity(Integer.valueOf(0));
            goods.setState(0);
        }
        if (goods.getGoodsId() == null) {
            //当商品ID为空，新增商品信息
            int rows = goodsDao.saveGoods(goods);
            if (rows > 0) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        } else {
            //当商品ID不为空，修改商品信息
            int rows = goodsDao.updateGoods(goods);
            if (rows > 0) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        }
        return null;
    }

    /*
     * 根据ID删除商品
     * */
    @Override
    public ServiceVO goodsDeleteById(Integer goodsId) {
        goodsDao.deleteGoods(goodsId);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 分页查询无库存商品信息
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(offSet, rows, nameOrCode);

        int size = goodsList.size();
        map.put("total", size);
        map.put("rows", goodsList);
        return map;
    }

    /**
     * 分页查询有库存商品信息
     */
    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(offSet, rows, nameOrCode);

        int size = goodsList.size();
        map.put("total", size);
        map.put("rows", goodsList);
        return map;
    }

    /*
     * 添加商品期初库存
     * */
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {

        int rows = goodsDao.saveStock(goodsId, inventoryQuantity, purchasingPrice);
        if (rows > 0) {
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        }
        return null;
    }

    /**
     * 删除商品库存
     *
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        Goods goods = goodsDao.getGoodsById(goodsId);
        if (goods.getState() == 0) {
            goods.setInventoryQuantity(0);
            //Integer inventoryQuantity = 0;
            //int rows = goodsDao.deleteStock(goodsId, inventoryQuantity);
            int rows = goodsDao.deleteStock(goods);
            if (rows > 0) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        }
        return null;
    }

    /**
     * 查询库存报警商品信息
     *
     * @return
     */
    @Override
    public Map<String, Object> listAlarm() {
        HashMap<String, Object> map = new HashMap<>();

        List<Goods> goodsList = goodsDao.listAlarm();

        map.put("rows", goodsList);

        return map;
    }


}
