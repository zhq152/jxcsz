package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@SuppressWarnings("all")
public class SupplierServiceImpl implements SupplierService {


    @Autowired
    private SupplierDao supplierDao;


    /*
     * 分页查询供应商
     * */
    @Override
    public Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName) {
        HashMap<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Supplier> supplierList = null;
        if (supplierName != null) {
            supplierList = supplierDao.getSupplierList(offSet, rows, supplierName);
        } else {
            supplierList = supplierDao.getSupplierListAll(offSet, rows);
        }
        /*List<Supplier> supplierList = supplierDao.getSupplierList(offSet, rows, supplierName);*/
        int size = supplierList.size();
        map.put("total", size);
        map.put("rows", supplierList);
        return map;
    }

    /*
     * 新增供应商
     * */
    @Override
    public ServiceVO save(Supplier supplier) {
        // 供应商ID为空时，说明是新增操作，
        Integer supplierId = supplier.getSupplierId();
        if (supplierId == null) {
            //新增客户信息
            int flag = supplierDao.save(supplier);
            if (flag > 0) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        } else {
            //更新客户信息
            int flag = supplierDao.update(supplier);
            if (flag > 0) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        }
        return null;
    }

    /*
     * 删除供应商
     * */
    @Override
    public ServiceVO delete(String ids) { //"13,14"
        String[] split = ids.split(","); //["13","14"]
        List<String> list = Arrays.asList(split);
        for (String supplierId : list) { //"13"
            supplierDao.deleteById(supplierId);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


}
