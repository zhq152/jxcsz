package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@SuppressWarnings("ALL")
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    /*
     * 获取客户列表（分页查询）
     * */
    @Override
    public Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName) {
        HashMap<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
      /*  List<Customer> CustomerList = null;
        if (customerName != null) {
            CustomerList = customerDao.getCustomerList(offSet, rows, customerName);
        } else {
            CustomerList = customerDao.getCustomerListAll(offSet, rows);
        }*/
        List<Customer> CustomerList = customerDao.getCustomerList(offSet, rows, customerName);

        int size = CustomerList.size();
        map.put("total", size);
        map.put("rows", CustomerList);

        return map;
    }

    @Override
    public ServiceVO save(Customer customer) {
        // 供应商ID为空时，说明是新增操作，
        Integer customerId = customer.getCustomerId();
        if (customerId == null) {
            int flag = customerDao.save(customer);
            if (flag > 0) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        } else {
            int flag = customerDao.update(customer);
            if (flag > 0) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        }
        return null;
    }

    @Override
    public ServiceVO delete(String ids) {
        String[] split = ids.split(",");
        List<String> list = Arrays.asList(split);
        for (String customerId : list) {
            customerDao.deleteById(customerId);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

}
