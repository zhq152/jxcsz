package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /*
    * 获取客户列表（分页查询）
    * */
    @PostMapping("/list")
    public Map<String,Object> getCustomerList(Integer page, Integer rows, String  customerName){
        return customerService.getCustomerList(page,rows,customerName);
    }

    /*
     * 新增/更新客户
     * */
    @PostMapping("/save")
    public ServiceVO save(Customer customer) {
        return customerService.save(customer);
    }

    /*
     * 删除客户
     * */
    @PostMapping("/delete")
    public ServiceVO delete(String ids) {
        return customerService.delete(ids);
    }

}
