package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.DamageService;
import com.atguigu.jxc.service.OverflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class DamageController {

    @Autowired
    private DamageService damageService;


    /*
     * 保存报损单
     * */
    @PostMapping("damageListGoods/save")
    public ServiceVO damageListGoodsSave(DamageList damageList, String damageListGoodsStr) {
        return damageService.damageListGoodsSave(damageList, damageListGoodsStr);
    }

    /*
     * 报损单查询
     * */
    @PostMapping("damageListGoods/list")
    public Map<String, Object> damageListGoods(String sTime, String eTime) {
        return damageService.damageListGoods(sTime, eTime);
    }

    /*
     * 查询报损单商品信息
     * */
    @PostMapping("damageListGoods/goodsList")
    public Map<String, Object> getGoodsList(Integer damageListId) {
        return damageService.getGoodsList(damageListId);
    }


}
