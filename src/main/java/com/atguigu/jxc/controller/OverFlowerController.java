package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class OverFlowerController {

    @Autowired
    private OverflowService overflowService;

    /*
     * 保存报溢单
     * */
    @PostMapping("overflowListGoods/save")
    public ServiceVO overflowListGoods(OverflowList overflowList, String overflowListGoodsStr) {
        return overflowService.overflowListGoods(overflowList, overflowListGoodsStr);
    }

    /*
     * 报溢单查询
     * */
    @PostMapping("overflowListGoods/list")
    public Map<String, Object> getOverflowList(String sTime, String eTime) {
        return overflowService.getOverflowList(sTime, eTime);
    }

    /*
     * 报溢单商品信息
     * */
    @PostMapping("overflowListGoods/goodsList")
    public Map<String, Object> getOverflowListGoods(Integer overflowListId) {
        return overflowService.getOverflowListGoods(overflowListId);
    }

}
