package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/supplier")
public class supplierController {

    @Autowired
    private SupplierService supplierService;

    /*
     * 分页查询供应商
     * */
    @PostMapping("/list")
    private Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName) {
        return supplierService.getSupplierList(page, rows, supplierName);
    }

    /*
     * 新增/更新供应商
     * */
    @PostMapping("/save")
    public ServiceVO save(Supplier supplier) {
        return supplierService.save(supplier);
    }

    /*
     * 删除供应商
     * */
    @PostMapping("/delete")
    public ServiceVO delete(String ids) {
        return supplierService.delete(ids);
    }

}
