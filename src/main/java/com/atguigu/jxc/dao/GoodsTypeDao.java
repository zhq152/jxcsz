package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    /*
     * 新增商品分类
     * */
    int addGoodsType(GoodsType goodsType);

    /*
     * 删除商品分类
     * */
    int deleteGoodsType(@Param("goodsTypeId") Integer goodsTypeId);
}
