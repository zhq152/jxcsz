package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowDao {


    /*
     * 保存报溢单
     * */
    void overflowList(OverflowList overflowList);

    /*
     * 保存报溢单
     * */
    void overflowListGoods(OverflowListGoods overflowListGood);

    /*
     * 报溢单查询
     * */
    List<OverflowList> getOverflowList(@Param("sTime") String sTime, @Param("eTime") String eTime);

    /*
     * 报溢单商品信息
     * */
    List<OverflowListGoods> getOverflowListGoods(@Param("overflowListId") Integer overflowListId);
}
