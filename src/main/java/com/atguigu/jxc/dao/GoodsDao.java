package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();

    /*带条件分页查询商品库存信息*/
    List<Goods> getGoodsList(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    /*不带条件分页查询商品库存信息*/
    List<Goods> getGoodsListAll(@Param("offSet") int offSet, @Param("rows") Integer rows);

    //查询
    /* List<Goods> getGoodsChild(String codeOrName, Integer goodsTypeId);*/

    /*
     * 查询所有商品单位
     * */
    List<Unit> getUnitList();

    /*
     * 保存商品信息
     * */
    int saveGoods(Goods goods);

    /*
     * 更新商品信息
     * */
    int updateGoods(Goods goods);

    /*
     * 删除商品信息
     * */
    void deleteGoods(Integer goodsId);

    /*
     *
     * 分页查询无库存商品信息
     */
    List<Goods> getNoInventoryQuantity(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    /*
     *
     * 分页查询有库存商品信息
     */
    List<Goods> getHasInventoryQuantity(int offSet, Integer rows, String nameOrCode);

    /*
     * 添加商品期初库存
     * */
    int saveStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") double purchasingPrice);

    /**
     * 删除商品库存
     */
    //int deleteStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity);
    int deleteStock(Goods goods);

    /*
     * 根据goodsId查询商品
     * */
    Goods getGoodsById(Integer goodsId);

    /*
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * */
    List<Goods> listAlarm();

    /*
     * 获取商品销售总数
     * */
    Integer getGoodsSaleTotalNum(@Param("goodsId") Integer goodsId);

    /*
     * 获取客户退回的商品总数
     * */
    Integer getCustomerReturnGoodsTotalNum(@Param("goodsId") Integer goodsId);
}
