package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {


    /*
     * 带条件查询
     * */
    List<Supplier> getSupplierList(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    /*
     * 查询全部
     * */
    List<Supplier> getSupplierListAll(@Param("offSet") int offSet, @Param("rows") Integer rows);

    /*
     * 新增供货商
     * */
    int save(Supplier supplier);

    /*
     * 修改供应商
     * */
    int update(Supplier supplier);

    /*
     * 删除供应商
     * */
    int deleteById(String supplierId);
}
