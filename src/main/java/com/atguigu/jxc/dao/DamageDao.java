package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageDao {

    /*
     * 保存报损单
     * */
    int damageListSave(DamageList damageList);

    /*
     * 保存报损单下面的物品列表
     * */
    void damageListGoodsSave(DamageListGoods damageListGood);


    /*
     * 报损单查询
     * */
    List<DamageList> damageList(@Param("sTime") String sTime, @Param("eTime") String eTime);

    /*
     * 查询报损单商品信息
     * */
    List<DamageListGoods> getGoodsList(@Param("damageListId") Integer damageListId);
}
