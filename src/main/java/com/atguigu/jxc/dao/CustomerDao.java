package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;

import java.util.List;

public interface CustomerDao {

    /*
     * 获取客户列表（分页查询）
     * */
    List<Customer> getCustomerList(int offSet, Integer rows, String customerName);

    /*新增客户信息*/
    int save(Customer customer);

    /*客户信息更新*/
    int update(Customer customer);

    /*删除客户信息*/
    void deleteById(String customerId);
}
